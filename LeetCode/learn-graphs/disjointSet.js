// Disjoint Set is a data structure also know as a Union find
// The primary use of disjoint sets is to address the connectivity between the components of a network. Nodes might not connect hence disjointed
// Key terms - Parent Node, Root Node === A Parent Node without a parent node, or Head Node
// If Nodes have the same root or head node, they are in the same set

// Simple Solution using an array, value and indexes. Need to keep track of head or parent node,
// The values in the array, are the parent nodes, the index is the vertex
// for example
// [0, 0, 0, 1] === value or parent nodes, this this, 3 parent node === 1, 2 === 0, 1 === 0, 0 === 0
// [0,1,2,3] === indexes
// to find a root node, find a node where parent === index, or 0 in this case
// when you union two sets, you need to choose the new parent node of the set, and then change the value of that index to the new parent

// Sample Question
// [0,0,0,1,4,5,5,5,4,9] = value
// [0,1,2,3,4,5,6,7,8,9] = indexes
// are (0,3) connected? Yes, because 3 parent, is 1, 1 parent is 0, 0 parent 0. i.e. they have same root node
// are (1,5) connected? No, 5 parent is 5, or a root node of 5. 1 root node is 0. root nodes !===

// How do we combine two? Change the parent of the root of a set, into another node set
// in this example, if we wanted to combine the set of (4*,8) union (5*,6,7)
// [0,0,0,1,4,5,5,5,4,9] -> [0,0,0,1,*7*,5,5,5,4,9] then index 4, is no longer 4, it is 7 and part of new set, we can see 5 is the new root node

// Two Main functions
//  Union === combine two nodes, choose one as parent, make changes on the array  === unions two vertices and makes their root nodes the same.
//  Find === find the root node

// Two ways to implement, Quick union or Quick find
// Much like quick find, but you dont do the final search of the array setting the rest of the root nodes that need to be udpated

// Quick Find, instead of storing parent nodes, we only store the root node relationship
// Makes union more slow, as after you update the root node of the new jointing node, you have to go through the entire array to find all the other nodes and update their root nodes to the new one
// Find === O(1) as the root nodes are all listed for each node

// Quick Union
//

// Union Rank
// To optimize for both cases, you need to implement Union Rank, have a better way of choosing the parent node
// Rank === Height of each vertex, merge the taller tree as the root node for both vertixes
// This avoids the possibility of connected all vertixes into a straight link by balancing the tree
// this optimization is called the “disjoint set” with union by rank.
// check root node of both trees, choose the node with the highest height as new node, the union of rank
// quickfind is still faster, but this is good for quick union disjoint sets, designed for the union functions

// Path compression optimizations, for the quick union find, when you traverse using the find function, you use recursion to set the root nodes as you find, so that the next time, all the child nodes are direct children of the root node

class QuickFind {
  // Constructor == O(N)
  // Find == O(1)
  // Union == O(N)
  // Connected = O(1)
  constructor(size) {
    this.roots = [...Array(size).keys()]
    // Iterate through entire array === O(N)
  }

  find(
    vertex // returns the root node of a vertex, O(1)
  ) {
    return this.roots[vertex]
  }
  union(
    vertex_x,
    vertex_y // first parameter becomes root
  ) {
    let root_x = this.find(vertex_x) // O(1)
    let root_y = this.find(vertex_y) // O(1)

    if (root_x !== root_y) {
      // O(N)
      for (let i = 0; i < this.roots.length; i++) {
        if (this.roots[i] === root_y) {
          this.roots[i] = root_x // if something is set to the old y root, set to new x root
        }
      }
    }
  }
  connected(vertex_x, vertex_y) {
    // find === O(1)
    return this.find(vertex_x) === this.find(vertex_y)
  }
}

class QuickUnion {
  // Constructor == O(N)
  // Find == O(N) // worst case is <= O(N) but not hit as often
  // Union == O(N)
  // Connected = O(N)
  constructor(size) {
    this.roots = [...Array(size).keys()]
    // Iterate through entire array === O(N)
  }

  find(
    vertex // loops until finds a root node  equal to the vertex, O(N)
  ) {
    while (vertex != this.roots[vertex]) {
      vertex = this.roots[vertex] // crawls array until root node is found
    }
    return vertex
  }
  union( // has 1 less for than the quick find union, its still O(N)
    vertex_x,
    vertex_y // first parameter becomes root
  ) {
    let root_x = this.find(vertex_x) // O(N)
    let root_y = this.find(vertex_y) // O(N)

    if (root_x !== root_y) {
      // O(1)
      this.roots[root_y] = root_x
    }
  }
  connected(vertex_x, vertex_y) {
    // find === O(N)
    return this.find(vertex_x) === this.find(vertex_y)
  }
}

class UnionByRank {
  // Constructor == O(N)
  // Find == O(log N)
  // Union == O(log N)
  // Connected = O(log N)

  constructor(size) {
    this.roots = [...Array(size).keys()]
    this.rank = Array(size).fill(1) // extra array to keep track of ranks or heights
    // Iterate through entire array twice === O(N)
  }

  find(
    vertex // loops until finds a root node  equal to the vertex, O(N)
  ) {
    while (vertex != this.roots[vertex]) {
      vertex = this.roots[vertex] // crawls array until root node is found
    }
    return vertex
  }
  union( // has 1 less for than the quick find union, its still O(N)
    vertex_x,
    vertex_y // whatever vertex has lower height becomes root, if same x becomes root
  ) {
    let root_x = this.find(vertex_x) // O(N)
    let root_y = this.find(vertex_y) // O(N)

    if (root_x !== root_y) {
      // O(N)
      if (this.rank[root_x] > this.rank[root_y]) {
        this.roots[root_x] = root_y
      } else if (this.rank[root_x] < this.rank[root_y]) {
        this.roots[root_y] = root_x
      } else {
        this.roots[root_y] = root_x
        this.rank[root_x] += 1 // need to increase the rank of the tree as one node is now a higher height compared to rest
      }
    }
  }
  connected(vertex_x, vertex_y) {
    // find === O(N)
    return this.find(vertex_x) === this.find(vertex_y)
  }
}

class QuickUnionCompressionOptimization {
  // Quick union but optimized path after 1st find.
  // Constructor == O(N)
  // Find == O(logN)
  // Union == O(logN)
  // Connected = O(logN)
  constructor(size) {
    this.roots = [...Array(size).keys()]
    // Iterate through entire array === O(N)
  }

  find(
    vertex // recursive function to set found root node to all the children that share the root node, minimizing the rank of the tree for those nodes
  ) {
    if (vertex === this.roots[vertex]) {
      return vertex
    }

    this.roots[vertex] = this.find(this.roots[vertex])
    return this.roots[vertex]
  }
  union( // has 1 less for than the quick find union, its still O(N)
    vertex_x,
    vertex_y // first parameter becomes root
  ) {
    let root_x = this.find(vertex_x) // O(N)
    let root_y = this.find(vertex_y) // O(N)

    if (root_x !== root_y) {
      // O(1)
      this.roots[root_y] = root_x
    }
  }
  connected(vertex_x, vertex_y) {
    // find === O(N)
    return this.find(vertex_x) === this.find(vertex_y)
  }
}

const timer = (testObject, size) => {
  console.log(`Testing: ${testObject.constructor.name}`)
  // const testObject = new objectClass(size)
  size = size - 1
  const then = Date.now()

  for (let i = 0; i < size / 2; i++) {
    testObject.union(
      Math.floor(Math.random() * size),
      Math.floor(Math.random() * size)
    )
  }
  testObject.union(1, 5)
  testObject.union(5, 7)

  const unionTime = Date.now()
  for (let i = 0; i < size / 10; i++) {
    testObject.find(Math.floor(Math.random() * size))
  }
  const findTime = Date.now()
  for (let i = 0; i < size / 10; i++) {
    testObject.connected(
      Math.floor(Math.random() * size),
      Math.floor(Math.random() * size)
    )
  }
  const connectedTime = Date.now()
  console.log(
    `Is 1 and 5 connected? Expect True - Results: ${testObject.connected(1, 5)}`
  )
  console.log(
    `Is 5 and 7 connected? Expect True - Results: ${testObject.connected(5, 7)}`
  )

  console.log(`Union Time: ${unionTime - then} ms`)
  console.log(`Find Time: ${findTime - unionTime} ms`)
  console.log(`Connected Time: ${connectedTime - findTime} ms`)
  console.log(`Total Time: ${connectedTime - then} ms`)
}

// Test
const size = 100000
const qFind = new QuickFind(size)
const qUnion = new QuickUnion(size)
const qUnionByRank = new UnionByRank(size)
const qUnionPathOptimize = new QuickUnionCompressionOptimization(size)
// timer(qFind, size)
// timer(qUnion, size)
// timer(qUnionByRank, size)
timer(qUnionPathOptimize, size)
